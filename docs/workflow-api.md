# Worflow api

## Оглавление
1. [Установка](#установка)
2. [Action](#action)
3. [State](#state)
4. [Custom fields](#custom-fields)
5. [Config](#config)
6. [Основной функционал](#основной-функционал)

## Установка
Перед установкой плагина требуется взять последнюю версию в zip формате
из папки zip-workflow, либо сгененрировать свою версию через команду
```bash
$ npm run compress:zip
```

После чего под админом зайти в личный кабинет youtrack в рабочие процессы.
И нажать на "импортировать рабочий процесс" и выбираем скачанный zip
![import](https://i.ibb.co/4dtCcfM/image-14.jpg)

В списке рабочих процессов должен появиться плагин
![plugin](https://i.ibb.co/881rq4C/image-15.jpg)

После чего в файле config можно кастомизировать процессы под доску
Конфиг раздлен на основные части

## Action
Action-ы представляют основные endpoint-ы которые отсылаются на развернутый webhook.
Не кастомизируются.
```text
const ACTIONS = {
  createMr: "create-mr", - создание merge_request
  createDevelopMr: 'create-develop-mr', - слив ветки в dev ветку
  createReleaseMr: 'create-release-mr', - создание release merge_request
  deleteReleaseMr: 'delete-release', - удаление merge_request
  updateReleaseIssue: 'update-release-tasks' - обновление release ветки
};
```

## State
State-ы это состояния на доске agile на которые будут вызываться Action-ы при передвижении
задачи в новое состояние. 
```text
const STATE = {
  review: 'Ревью', - срабатывает создание merge_request
  release: 'В очереди на релиз' - срабатывает слив ветки в dev ветку
};
```
Название состояний можно кастомизировать для этого достаточно поменять их назавния в соответсвии с вашим проектом.

## Custom fields
Представляют из себя кастомные поля в задачах на проекте. Название полей можно кастомизировать.
```text
const BRANCH_FIELDS = {
  developBranch: 'Develop ветка', - здесь указывается название ветки куда будет сливаться задача после тестирования. 
  От этой ветки будет создаваться stageBranch если он не создан!
  stageBranch: 'Stage ветка', - здесь указывается название ветки куда будет формироваться source_branch в gitlab после создания merge_request на review
  masterBranch: 'Master ветка' - здесь указывается название ветки куда будет формироваться source_branch в gitlab после создания merge_request на release.
  От этой ветки будет создаваться developBranch, releaseBranch если он не создан!
};

const RELEASE_BRANCH_NAME = 'release' - префикс для релизной ветки;
```

## CONFIG
Здесь описаны внутренние процессы работы плагина
```text
const CONFIG = {
  baseUrl: 'https://youtrack-dmitryitis.amvera.io/', - ссылка на развернутый webhook
  releaseTag: 'epic', - тег используется для родительской задачи, название можно кастомизировать. Без тега в задаче невозможно будет создать релизную ветку!
  versionField: 'Версия релиза', - кастомное поле которое важно заполнить в родительской задачи для формирование релизной ветки формата release/{1.0.0
  states: [{
    state: STATE.review,
    stageBranch: BRANCH_FIELDS.stageBranch,
    developBranch: BRANCH_FIELDS.developBranch,
    action: ACTIONS.createMr,
    excludeTags: ['epic'] - указаны теги при которых рабочий процесс не будет срабатывать при изменени состояния задачи.
  },
   {
    state: STATE.release,
    action: ACTIONS.createDevelopMr,
    masterBranch: BRANCH_FIELDS.masterBranch,
    developBranch: BRANCH_FIELDS.developBranch,
    excludeTags: ['epic']
   }     
  ], - здесь перечисляются все состояния при которых будут срабатывать скрипты
};
```

## Основной функционал

### test-connection-gitlab
После импорта рабочих процессов в задачах появиться следующая команда test-connection-gitlab
![description](https://i.ibb.co/D50tbJk/image-16.jpg)

Данная команда проверяет подключение к webhook и к репозиторию в gitlab.
![](https://i.ibb.co/G2pZ7Qn/image-17.jpg)

### Create release
Данная команда появляется в задачах помеченных тегом **epic** - либо другим кастомным тегом настроенным в конфиге.
![](https://i.ibb.co/xLSHqn2/image-18.jpg)
При нажатии будет сформирован release mr.
Для того чтобы release mr заполнился задачами требуется в родительской залачи указать связи к дочерним задачам. Важно дочерние задачи должны находиться в стадии STATE.release.
Иначе они не будут добавлены в релизную ветку.
![](https://i.ibb.co/YZMvy2V/image-19.jpg)

### State.review
Срабатывает при перемещение задачи в State.review. Здесь важно корректно заполнить BRANCH_FIELDS.stageBranch - она станет target_branch в gitlab и BRANCH_FIELDS.developBranch.
После корректного срабатывания в комментариях к задаче появиться ссылка на mr
![](https://i.ibb.co/MPxg2G0/image-20.png)