# [1.0.0](https://gitlab.com/helpme-group/plugin-youtrack/-/tags/1.0.0) (2024-04-20)


### Features

- **create-mr:** добавлен сценарий создания merge_request ([022a138](https://gitlab.com/helpme-group/plugin-youtrack/-/commit/022a138582741f34c63aae2e08b3549f70893b29))
- **merge in develop** добавлен сценарий мерджа ветки в dev ветку ([746deb4f](https://gitlab.com/helpme-group/plugin-youtrack/-/commit/746deb4fba90c3cc2852423d6d73267b1b07723d))
- **create release** добавлен сценарий создание релизной ветки ([c5dbacad](https://gitlab.com/helpme-group/plugin-youtrack/-/commit/c5dbacad88406a323c3a6df5e8873ba27a9ba055))
