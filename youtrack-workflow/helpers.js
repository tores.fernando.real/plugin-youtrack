
const isExcludeIssue = (issue, excludeTags) => {
  let result = false;
  
  for (let tag of excludeTags) {
    if (issue.hasTag(tag)) {
      result = true;
      break;
    }
  }
  
  return result;
};

exports.isExcludeIssue = isExcludeIssue;