# plugin-youtrack-gitlab ⚡

> Автоматизация процессов между youtrack и gitlab

Plugin-youtrack-gitlab - это плагин разработанный для автоматизации gitflow процессов
через youtrack.
Плагин разделен на две части.
1)  youtrack-workflow - набор скриптов для youtrack рабочих процессов, разработанный для соединения с api gitlab
2)  youtrack-gitlab-api - gitlab api написанное на flask позволяющее работать с gitlab репозиторием

## Оглавление
1. [Что доступно в плагине?](#что-доступно-в-плагине)
2. [Документация](#документация)
3. [Технологии](#технологии)
4. [Поднятие plugin webhook удаленно](#поднятие-plugin-webhook-удаленно)
5. [Генерация zip архива youtrack-workflow](#генерация-zip-архива-youtrack-workflow)
6. [Локальный запуск youtrack_gitlab_api](#локальный-запуск-youtrack_gitlab_api)
7. [Changelog](#changelog)

## Что доступно в плагине?
На данный момент в плагине реализованы следующие фичи:
- [X] Автоматическая генерация merge_request-ов
- [X] Автоматический мердж задач в dev ветки
- [X] Формирование релизных merge request-ов
- [X] Почтовая рассылка по SMTP

## Документация

Ознакомиться с документацией проекта вы сможете в [docs-youtrack-gitlab.io](https://gitlab.com/helpme-group/plugin-youtrack/-/blob/master/docs/index.md)

## Технологии

- Python, Flask
- Javascript
- Docker

## Поднятие plugin webhook удаленно
Версия плагина публикуется в [Docker hub](https://hub.docker.com/r/qwer342/youtrack-gitlab-api).
Подтянуть образ можно следующим образом:
```docker
docker pull qwer342/youtrack-gitlab-api
```

## Генерация zip архива youtrack-workflow
Набор скриптов для youtrack рабочих процессов лежит в папке zip-workflow.
Достаточно скачать последнюю версию и импортировать её в youtrack.
Если требуется сгенерировать новую версию, то нужно:

Установить зависимости
```bash
$ npm ci
```
Запустить скрипт
```bash
$ npm run compress:zip
```
Сгенерированная версия автоматически сформируется в папке zip-workflow под последней версией указанной 
в package.json
```json
{
  "name": "youtrack-workflow",
  "version": "1.0.0",
 }
```

## Локальный запуск youtrack_gitlab_api
Установка библиотек python
```bash
$ python -r requirements.txt
```
Активация venv
```bash
$ cd venv/Scripts && activate
```
Создание .env файла и установка обязательных .env переменных.
Все используемые переменные можно найти в .env.example
```bash
GITLAB_URL=<key> - gitlab url
PROJECT_TOKEN=<key> - токен авторизации
PROJECT_ID=<key> - id проекта требующий автоматизации
PORT=5000 - port на котором поднимется приложение
```
Запуск проекта
```bash
$ python app.py
```

## Changelog
Подробные изменения для каждого релиза задокументированы в [Changelog.md](https://gitlab.com/helpme-group/plugin-youtrack/-/blob/master/CHANGELOG.md).