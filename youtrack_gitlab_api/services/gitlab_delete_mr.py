from youtrack_gitlab_api.utils.helpers import get_opened_merge_requests
import logging
from youtrack_gitlab_api.constants import Status


def delete_merge_request_if_exists(project, kwargs):
    try:
        all_opened_mrs = get_opened_merge_requests(project)
        existing_mr = None

        for opened_mr in all_opened_mrs:
            if (opened_mr.source_branch == kwargs['source_branch']) and (
                    opened_mr.target_branch == kwargs['target_branch']):
                existing_mr = opened_mr
        if existing_mr is not None:
            existing_mr.delete()
        return {'status': Status.SUCCESS, 'message': "MR удалён"}
    except Exception as e:
        logging.error(e)
        return {'status': Status.ERROR}
