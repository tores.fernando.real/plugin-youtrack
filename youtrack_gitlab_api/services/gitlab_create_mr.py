from youtrack_gitlab_api.utils.helpers import get_opened_merge_requests, wait_mr_pass_checking, get_branches_by_name
from youtrack_gitlab_api.constants import Status
import logging


def create_merge_request_if_not_exists(project, kwargs):
    try:
        existed_branches = get_branches_by_name(project, kwargs["source_branch"])
        if len(existed_branches) == 0:
            return {"status": Status.ERROR, 'message': f'{kwargs["source_branch"]} данной ветки не существует в репозитории!'}
        all_opened_mrs = get_opened_merge_requests(project)
        existing_mr = None

        for opened_mr in all_opened_mrs:
            if (opened_mr.source_branch == kwargs['source_branch']) and (
                    opened_mr.target_branch == kwargs['target_branch']):
                existing_mr = opened_mr
                break

        if existing_mr is None:
            new_mr = project.mergerequests.create(
                {'source_branch': kwargs['source_branch'], 'target_branch': kwargs['target_branch'],
                 'title': f"{kwargs['title']}", 'description': kwargs['description']})
            wait_mr_pass_checking(project, new_mr)
            return {"status": Status.SUCCESS, 'mr': new_mr}
        else:
            return {"status": Status.CREATED, 'mr': existing_mr}
    except Exception as e:
        logging.error(e)
        return {"status": Status.ERROR, 'message': f'Не удалось создать mr для {kwargs["source_branch"]}!'}
