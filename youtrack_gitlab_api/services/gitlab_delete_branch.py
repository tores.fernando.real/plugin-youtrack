from youtrack_gitlab_api.utils.helpers import check_branches_exists_regex
import logging
from youtrack_gitlab_api.constants import Status


def delete_branch_if_exists(project, kwargs):
    try:
        branches = check_branches_exists_regex(project, kwargs['source_branch'])

        if len(branches) > 0:
            for branch in branches:
                del_branch = project.branches.get(branch)
                del_branch.delete()
        return {'status': Status.SUCCESS, 'message': 'Ветки удалены'}
    except Exception as e:
        logging.error(e)
        return {'status': Status.ERROR}
