from enum import Enum

MR_CHECK_TIMEOUT = 2
MR_CHECK_INTERVAL = 60


class Status(Enum):
    SUCCESS = 'success'
    ERROR = 'error'
    CREATED = 'created'
