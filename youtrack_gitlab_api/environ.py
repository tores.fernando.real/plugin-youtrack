from environs import Env

env = Env()
env.read_env()

GITLAB_URL = env("GITLAB_URL")
PROJECT_TOKEN = env("PROJECT_TOKEN")
PROJECT_ID = env("PROJECT_ID")
PORT = env("PORT")
DEBUG = env("DEBUG")
SMTP_SERVER = env("SMTP_SERVER")
SMTP_PASSWORD = env("SMTP_PASSWORD")
SMTP_HOST = env("SMTP_HOST")
SMTP_PORT = env("SMTP_PORT")